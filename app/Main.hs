{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import qualified Csg
import qualified Csg.STL
import qualified Data.Text.IO as T

import qualified Servo
import qualified ServoMount
import qualified ArmJoint
import qualified BaseJoint
import qualified Base
import qualified Herringbone
import qualified Grabber
import qualified ControlPannel


writeObject :: Csg.BspTree -> String -> IO ()
writeObject obj filename = do
    putStrLn $ "writing " ++ filename
    T.writeFile filename $ Csg.STL.toSTL obj
    putStrLn $ "written " ++ filename
jointWithServo = Csg.union Servo.servo ArmJoint.joint
armJointTest = jointWithServo `Csg.union` (Csg.translate (0, 40, 0) $ Csg.rotate (0, 0, 1) (pi/4) jointWithServo)

herringboneTestSpec = Herringbone.Spec 14 16 8 (pi/16) 5
herringboneTest = gear `Csg.union` (Csg.translate (-16, 4, 0) rack)
  where 
    rack = Herringbone.rack 60 herringboneTestSpec
    gear = Herringbone.gear herringboneTestSpec
    

main :: IO [()]
main = sequence $ (uncurry writeObject) <$> [
        (ControlPannel.controlPannel, "control_pannel.stl"),
        (ControlPannel.assembly, "control_assembly.stl"),
        (Servo.servo, "servo.stl"),
        (ServoMount.rotaryServoMount, "mount.stl"),
        (ArmJoint.joint, "arm_joint.stl"),
        (Grabber.grabber, "grabber.stl"),
        (Grabber.clawHalf, "grabber_claw.stl"),
        (Grabber.gear, "grabber_gear.stl"),
        (Grabber.gripper, "grabber_gripper.stl"),
        (BaseJoint.joint, "base_joint.stl"),
        (Base.armBase, "arm_base.stl"), 
        (Grabber.assembly, "grabber_assembly_test.stl"),
        (herringboneTest, "herringbone_test.stl"),
        (armJointTest, "arm_joint_test.stl")
    ]
