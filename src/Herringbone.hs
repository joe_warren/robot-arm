module Herringbone (gear, rack, Spec (..)) where

import qualified Csg

data Spec = Spec {
    radius :: Double,
    spokes :: Int,
    thickness :: Double,
    rotation :: Double,
    nSteps :: Int
    }
    

gear :: Spec -> Csg.BspTree
gear s = half `Csg.union` (Csg.scale (1, 1, -1) half)
  where 
    steps = nSteps s
    sliceThickness = (thickness s)/(2*(fromIntegral steps))
    cyl = Csg.scale (radius s, radius s, sliceThickness) $ Csg.unitCylinder 32
    spokeW = (radius s) * pi * 2 / (fromIntegral $ spokes s)
    omega = atan $ ((rotation s) * (radius s))/(sliceThickness*fromIntegral steps)
    spokeCube = Csg.rotate (-1, 0, 0) omega $ Csg.translate (radius s, 0, 0) $ Csg.scale (spokeW / sqrt 2, spokeW/sqrt 2, (thickness s)) $Csg.rotate (0, 0, 1) (pi/4) Csg.unitCube
    spokeCubes = Csg.unionConcat $ (\i -> Csg.rotate (0, 0, 1) (2 * pi * (fromIntegral i) / (fromIntegral $ spokes s)) spokeCube) <$> [1..spokes s]
    slice  = Csg.translate (0, 0, 0.25 * (thickness s) / (fromIntegral steps)) $  cyl `Csg.subtract` spokeCubes
    half = Csg.unionConcat $ (\i -> Csg.translate (0, 0, (fromIntegral i)*(thickness s)/(2 * (fromIntegral steps))) $ Csg.rotate (0, 0, 1) ((fromIntegral i) * (rotation s) /(fromIntegral steps)) slice ) <$> [0..(steps-1)]
    
rack :: Double -> Spec -> Csg.BspTree
rack length s = half `Csg.union` (Csg.scale (1, 1, -1) half)
  where
    steps = nSteps s
    sliceThickness = (thickness s)/(2*(fromIntegral steps))
    omega = atan $ ((rotation s) * (radius s))/(sliceThickness*fromIntegral steps)
    spokeW = (radius s) * pi * 2 / (fromIntegral $ spokes s)
    spokeCube = Csg.rotate (1, 0, 0) omega $ Csg.translate (0.5 * thickness s, 0, 0) $ Csg.scale (spokeW / sqrt 2, spokeW/sqrt 2, 2*(thickness s)) $Csg.rotate (0, 0, 1) (pi/4) Csg.unitCube
    shape = Csg.scale (thickness s, length, 0.5 * thickness s) $ Csg.unitCube
    spokesCombined = Csg.translate (0, -length/2, 0) $ Csg.unionConcat $ (\i -> Csg.translate (0, (fromIntegral i) * spokeW, 0) spokeCube) <$> [0 .. (ceiling (length/spokeW))]
    half = Csg.translate (0, 0, 0.25 * thickness s) $ shape `Csg.subtract` spokesCombined
    
