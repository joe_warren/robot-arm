module ArmJoint (joint, jointCurve) where

import qualified Csg
import qualified ServoMount

joint :: Csg.BspTree
joint = ServoMount.rotaryServoMount `Csg.union` jointCurve

jointCurve :: Csg.BspTree
jointCurve = positives `Csg.subtract` negatives
  where
    pivotY = 40
    topPivotOffset = (0.0, pivotY, 10.0)
    bottomPivotOffset = (0.0, pivotY, 10.0)
    midBraceDims = (12+4, 2, 18.5+2)
    midBraceOffset = (0, braceStartY + 1, -(18.5+2)/2)
    midBrace = Csg.translate midBraceOffset $ Csg.scale midBraceDims Csg.unitCube
    braceStartY = 32/2+5.5
    braceCurveRadius = 12
    braceBottomMidpointY = (pivotY + braceStartY)/2
    braceTopMidpointY = (pivotY + braceStartY+braceCurveRadius)/2
    braceBottomDims = (12+4, pivotY-braceStartY, 2)
    braceBottomOffset = (0, braceBottomMidpointY, -(18.5 + 2 +0.5 + 1))
    braceBottom = Csg.translate braceBottomOffset $ Csg.scale braceBottomDims Csg.unitCube
    braceTopDims = (12+4, pivotY - (braceStartY +braceCurveRadius), 2)
    braceTopOffset = (0, braceTopMidpointY, 10+1)
    braceTop = Csg.translate braceTopOffset $ Csg.scale braceTopDims Csg.unitCube
    braceTopCurveDims = (braceCurveRadius, braceCurveRadius, 12+4)
    braceTopCurveInnerDims = (braceCurveRadius-2, braceCurveRadius-2, (12+4)*2)
    braceTopCurveOuter = Csg.scale braceTopCurveDims $ Csg.unitCylinder 32
    braceTopCurveInner = Csg.scale braceTopCurveInnerDims $ Csg.unitCylinder 32
    braceTopCurveUnmasked = Csg.subtract braceTopCurveOuter braceTopCurveInner
    braceTopCurveMask = Csg.uniformScale 2 $ Csg.scale braceTopCurveDims $ Csg.translate (-0.5, -0.5, 0) Csg.unitCube
    braceTopCurveUnrotated = Csg.intersection braceTopCurveMask braceTopCurveUnmasked
    braceTopCurve = Csg.translate (0, braceStartY+braceCurveRadius, 0) $  Csg.rotate (0, 1, 0) (pi/2) braceTopCurveUnrotated
    braceBottomJoinerDims = (2.5, 2.5, 12+4)
    braceBottomJoinerOffset = (0, braceStartY,-(18.5+2))
    braceBottomJoinerMask = Csg.uniformScale 2 $ Csg.translate (0.5, -0.5, 0) Csg.unitCube
    braceBottomJoinerCircle = Csg.rotate (0, 1, 0) (pi/2) $ Csg.scale braceBottomJoinerDims $ Csg.intersection braceBottomJoinerMask $ Csg.unitCylinder 16
    braceBottomJoinerSquare = Csg.translate (0, 1, -1) $ Csg.scale (12+4, 2, 2) Csg.unitCube
    braceBottomJoiner =  Csg.translate braceBottomJoinerOffset $ braceBottomJoinerCircle `Csg.union` braceBottomJoinerSquare
    pivotOuterCircleDims = ((12 + 4)/2, (12 +4)/2, 2)
    pivotOuterCircle = Csg.scale pivotOuterCircleDims $ Csg.unitCylinder 32
    pivotOuterCirclesOffsets = (\z -> (0, pivotY, z)) <$> [10+1, -(18.5 + 2 + 0.5 + 1)]
    pivotOuterCircles = Csg.unionConcat $ (Csg.translate <$> pivotOuterCirclesOffsets) <*> [pivotOuterCircle]
    anchorCrossWidth = 3.6
    anchorDims = (12+4-0.01, pivotY - (braceStartY +braceCurveRadius)- anchorCrossWidth/2, 4)
    anchorOffset = (0, braceTopMidpointY-anchorCrossWidth/4, 10+2+2)
    anchor = Csg.translate anchorOffset $ Csg.scale anchorDims Csg.unitCube
    positives = Csg.unionConcat [midBrace, braceBottom, braceTop,  pivotOuterCircles, anchor, braceTopCurve, braceBottomJoiner]
    bottomPivotHoleDims = (4, 4, 3)
    bottomPivotHoleOffset = (0, pivotY, -(18.5 + 2 + 0.5 +1))
    bottomPivotHole = Csg.translate bottomPivotHoleOffset $ Csg.scale bottomPivotHoleDims $ Csg.unitCylinder 32
    topPivotHoleDims = (4, 4, 10)
    topPivotHoleOffset = (0, pivotY, 10+1)
    topPivotHole = Csg.translate topPivotHoleOffset $ Csg.scale topPivotHoleDims $ Csg.unitCylinder 32
    anchorGapDims = (3.5*(sqrt 2),50/(sqrt 2), 4)
    anchorGapOffset = (0, pivotY, 10+2+2)
    anchorGap = Csg.translate anchorGapOffset $ Csg.scale anchorGapDims $Csg.rotate (0, 0, 1) (pi/4) Csg.unitCube
    negatives = Csg.unionConcat [anchorGap, bottomPivotHole, topPivotHole]
