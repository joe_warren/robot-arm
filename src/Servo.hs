module Servo (servo) where

import qualified Csg

servo :: Csg.BspTree
servo = Csg.union shaftPlusAxel $ Csg.translate centerOffset boxBracedWHoles
  where
    blockDims = (12, 22.5, 22.5) 
    block = Csg.scale blockDims $ Csg.translate (0, 0, -0.5) Csg.unitCube
    braceDims = (12, 32, 2.66) 
    brace = Csg.scale braceDims $ Csg.translate (0, 0, 0.5) Csg.unitCube
    braceOffset = (0, 0, 4)
    midCylinderDims = (2.5, 2.5, 9.8 *2) 
    midCylinder = Csg.scale midCylinderDims $ Csg.unitCylinder 16
    boxBraced = Csg.unionConcat [(Csg.translate braceOffset block), brace, midCylinder]
    holeDims = (1.15, 1.15, 20)
    singleHole = Csg.scale holeDims $ Csg.unitCylinder 8
    holePositions = [(0, 27.5*i, 0) | i <- [0.5, -0.5]]
    positionedHoles = (flip Csg.translate) singleHole <$> holePositions
    cuts = (Csg.scale (1.3, 100, 100) Csg.unitCube) `Csg.subtract` (Csg.scale (200, 27.5, 200) Csg.unitCube)
    boxBracedWHoles = Csg.subtract boxBraced $ Csg.unionConcat (cuts:positionedHoles)
    shaftDims = (6, 6, 10*2)
    shaft = Csg.scale shaftDims $ Csg.unitCylinder 32
    axelDims = (2.25, 2.25, 14*2)
    axel = Csg.scale axelDims $ Csg.unitCylinder 16
    shaftPlusAxel = Csg.union shaft axel
    centerOffset = (0, 5.5, 0)
     
