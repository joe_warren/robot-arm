module Base (armBase) where

import qualified Csg

import qualified ServoMount

armBase :: Csg.BspTree
armBase = ((ServoMount.servoMount `Csg.union` top `Csg.union` base `Csg.union` rim) `Csg.intersection` bevelMask) `Csg.subtract` bearingTrack
  where 
    topR = 40
    topNoHole = Csg.scale (topR, topR, 6) $ Csg.translate (0, 0, -0.5) $ Csg.unitCylinder 32 
    ((x1, y1, z1), (x2, y2, z2)) = Csg.aabb ServoMount.servoMount
    servoDim = (x2 - x1, y2 - y1, z2 - z1)
    servoMid = ((x1 + x2)/2, (y1 + y2)/2, (z1 + z2)/2)
    servoMountHole = Csg.translate servoMid $ Csg.scale servoDim Csg.unitCube
    top = topNoHole `Csg.subtract` servoMountHole
    base = Csg.translate (0, 0, z1) $ Csg.scale (topR, topR, 2) $ Csg.translate (0, 0, 0.5)  $ Csg.unitCylinder 32
    rimOuter = Csg.scale (topR, topR, z1) $ Csg.translate (0, 0, 0.5) $ Csg.unitCylinder 32
    rimInner = Csg.scale (topR-2, topR-2, z1) $ Csg.translate (0, 0, 0.5) $ Csg.unitCylinder 32
    rimNoHoles = rimOuter `Csg.subtract` rimInner
    holesR = (-z1 - 2 - 6)/2
    holesZ = 6 + holesR
    holeCyl = Csg.translate (0, 0, -holesZ) $ Csg.rotate (1, 0, 0) (pi/2) $ Csg.scale (holesR, holesR, topR*3) $ Csg.unitCylinder 16
    holeRotations = [i * pi/6| i <- [0..5]]
    holes = Csg.unionConcat $ (flip (Csg.rotate (0, 0, 1)) holeCyl) <$> holeRotations
    rim = rimNoHoles `Csg.subtract` holes
    bevelMaskScale = (-z1 + topR - 4)
    bevelMask = Csg.translate (0, 0, 0.01+z1) $ Csg.uniformScale bevelMaskScale $ Csg.translate (0, 0, 0.5) $  Csg.unitCone 32 
    bearingInnerR = 27 + 1
    bearingOuterR = bearingInnerR + 7 + 0.5
    bearingTrackH = 1
    bearingOuterTrackDims = (bearingOuterR, bearingOuterR, bearingTrackH*2)
    bearingInnerTrackDims = (bearingInnerR, bearingInnerR, bearingTrackH*3)
    bearingTrack = (Csg.scale bearingOuterTrackDims $ Csg.unitCylinder 32) `Csg.subtract` (Csg.scale bearingInnerTrackDims $ Csg.unitCylinder 32)

