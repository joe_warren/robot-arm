module ServoMount (servoMount, rotaryServoMount) where

import qualified Csg

servoMount :: Csg.BspTree
servoMount = Csg.translate offset $ positives `Csg.subtract` negatives
  where 
    zAlignedUnit = Csg.translate (0, 0, -0.5) Csg.unitCube
    screwToDims = (12 + 4, 32, 6)
    screwTo = Csg.scale screwToDims zAlignedUnit
    surroundDims = (12+4, 22.5, 18.5+2)
    surround = Csg.scale surroundDims zAlignedUnit
    bottomDims = (12+4, (32-22.5), 2)
    bottomOffset = (0, 22.5/2, -18.5)
    bottom = Csg.translate bottomOffset $ Csg.scale bottomDims zAlignedUnit
    positives = Csg.unionConcat [screwTo, surround, bottom]
    holeDims = (12+0.5, 22.5+0.5+0.5, 18.5*2+0.5)
    hole = Csg.scale holeDims Csg.unitCube
    screwDims = (1, 1, 6.5*2)
    singleScrew = Csg.scale screwDims $ Csg.unitCylinder 8
    screwPositions = [(0, 27.5*i, 0) | i <- [0.5, -0.5]]
    positionedScrews = (flip Csg.translate) singleScrew <$> screwPositions
    wireGapDims = (6, 3, 14)
    wireGapOffset = (0, -22.5/2, 0)
    wireGap = Csg.translate wireGapOffset $ Csg.scale wireGapDims Csg.unitCube
    negatives = Csg.unionConcat $ [hole, wireGap] ++ positionedScrews
    offset = (0, 5.5, 0)

rotaryServoMount :: Csg.BspTree 
rotaryServoMount = Csg.unionConcat [servoMount, axel, spacer]
  where 
    axelDims = (3.9, 3.9, 4.5)
    axelOffset = (0, 0, -18.5-0.25-4.5/2) 
    axel = Csg.translate axelOffset $ Csg.scale axelDims $ Csg.unitCylinder 32
    spacerDims = (6, 6, 1)
    spacerOffset = (0, 0, -18.5-2) 
    spacer = Csg.translate spacerOffset $ Csg.scale spacerDims $ Csg.unitCylinder 32

