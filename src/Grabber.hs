module Grabber (grabber, gear, clawHalf, gripper, assembly) where

import qualified Csg
import qualified ArmJoint
import qualified ServoMount
import qualified Servo
import qualified Herringbone as H

gearSpec = H.Spec {
    H.radius = 8,
    H.spokes = 10,
    H.thickness = (5 + 3),
    H.rotation = (pi/14), 
    H.nSteps = 5
}
gear :: Csg.BspTree
gear = Csg.translate (0, 0, (H.thickness gearSpec)/2) (H.gear gearSpec) `Csg.subtract` hole
  where
    mainHole = Csg.scale (4.5/2, 4.5/2, 4*2) $ Csg.unitCylinder 32
    screwHole = Csg.scale (1, 1, 10*2) $ Csg.unitCylinder 8
    hole = mainHole `Csg.union` screwHole

clawHalf :: Csg.BspTree
clawHalf = clawArm `Csg.union` Csg.translate (-8-1.5, 0, (H.thickness gearSpec)/2) rack 
  where
    rack = H.rack 45 gearSpec
    intersectingCylinders = Csg.fromIntersection $ ((Csg.Intersect . Csg.translate (-0.5, 0, 0)) <> (Csg.Intersect . Csg.translate (0.5, 0, 0))) $ Csg.unitCylinder 32
    halfCylinders = intersectingCylinders `Csg.intersection` Csg.translate (0, 0.5, 0) Csg.unitCube
    height = 40
    (_, (_, dy, _)) = Csg.aabb halfCylinders
    positionedCylinders = Csg.translate 
                                (0,
                                 45/2 - 5,
                                (H.thickness gearSpec)-(2.5 * sin (pi/20))) $ 
                            Csg.rotate (1, 0, 0) (pi/2 + pi/20) $
                            Csg.scale (2 * (8 + 1.5+ 2 + 4), height/dy,  5) halfCylinders
    gripperMountCube = Csg.scale (5, 5 ,20) $ Csg.translate (0, -0.5, -0.5) Csg.unitCube
    gripperMount = gripperMountCube  `Csg.union` ( Csg.unionConcat $
                    (\z ->  Csg.translate (0, -5, z) $ 
                            Csg.rotate (1, 0, 0) (pi/2) $
                            Csg.scale (1.5, 1.5, 3) $
                                Csg.unitCylinder 12) 
                        <$> [-2.5, -10, 2.5-20])

    gripperMountTranslated = Csg.translate (0 ,
                                45/2 -5 - (height) * sin (pi/20) + (2.5 * cos (pi/20)),
                                ((H.thickness gearSpec) +  height* cos (pi/20)) - 3)
                                    gripperMount
    clawArmTopMask= Csg.translate (0 , 0,
                                ((H.thickness gearSpec) +  height* cos (pi/20)) - 3) $
                                    Csg.uniformScale 100 $ Csg.translate (0, 0, 0.5) $
                                        Csg.unitCube
    centralCylinderMask = Csg.translate (0, 0, H.thickness gearSpec) $
                            Csg.rotate (1, 0, 0) (pi/2) $
                            Csg.scale (8, 8, 100) $
                            Csg.unitCylinder 32

    guideMask = (Csg.scale ((8 + 1.5 + 4 + 2)*2, 100, 2*(H.thickness gearSpec)+4) $ Csg.unitCube) 
                    `Csg.subtract` (Csg.scale ((8 + 1.5 + 2)*2, 200, 100) $ Csg.unitCube)

    bottomMask = Csg.translate (0, 0, H.thickness gearSpec) $
                    Csg.uniformScale 100 $ 
                    Csg.translate (0, 0, -0.5) Csg.unitCube

    masks = Csg.unionConcat [clawArmTopMask, centralCylinderMask, guideMask, bottomMask]
    clawArm = (gripperMountTranslated `Csg.union` positionedCylinders) `Csg.subtract` masks

gripper :: Csg.BspTree 
gripper = gripperHoles `Csg.subtract` coneStack
  where
    gripperCube = Csg.scale (5, 5 ,20) $ Csg.translate (0, -0.5, -0.5) Csg.unitCube
    gripperHoles = gripperCube  `Csg.subtract` ( Csg.unionConcat $
                    (\z ->  Csg.translate (0, -5, z) $ 
                            Csg.rotate (1, 0, 0) (pi/2) $
                            Csg.scale (1.5, 1.5, 3) $
                                Csg.unitCylinder 12) 
                        <$> [-2.5, -10, 2.5-20])
    doubleCone = Csg.unionConcat $ 
                 [id, Csg.scale (1, 1, -1)] <*>
                 [Csg.translate (0, 0, 0.5) $ Csg.unitCone 32]
    scaledCone = Csg.translate (0, 7.5-2.5, 0) $ Csg.uniformScale 7.5 doubleCone
    coneStack = Csg.unionConcat [ Csg.translate (0, 0, fromIntegral (-z*2)) scaledCone | z <- [0..10] ]
    
    

rotateToServoMount :: Csg.BspTree -> Csg.BspTree
rotateToServoMount obj = Csg.translate (0, 1 +1.5, -4) $ Csg.rotate (0, 1, 0) pi $ Csg.rotate (1, 0, 0) (pi/2) obj

positionedGear :: Csg.BspTree
positionedGear = rotateToServoMount $ Csg.translate (0, 0, 10.5) gear

claws = rotateToServoMount (pc `Csg.union` pcr)
  where 
    pc = Csg.translate (0, 0, 10.5)  clawHalf
    pcr = Csg.rotate (0, 0, 1) pi pc

assembly :: Csg.BspTree
assembly = Csg.unionConcat [grabber, positionedGear, claws, rotateToServoMount Servo.servo]

grabber :: Csg.BspTree
grabber = ArmJoint.jointCurve `Csg.union` rotatedMount
  where 
    bottomAbsentBit = Csg.translate (0, 22.5/2+ 5.5, -18.5) $ Csg.uniformScale 20 $ Csg.translate (0, 0.5, 0) Csg.unitCube 
    unrotatedMount =ServoMount.servoMount `Csg.subtract` bottomAbsentBit
            
    clawMask = Csg.translate (0, 0, 10.5 + (H.thickness gearSpec)/2) $ Csg.scale ((8 + 1.5 + 4)*2, 50, 8)  Csg.unitCube 
    clawHolderUnmasked = Csg.translate (8, 5.5, 0) $ Csg.scale (1.5 + 4 + 2, 22.5, 8 + 10.5) $ Csg.translate (0.5, 0, 0.5) Csg.unitCube 
    clawHolderTop = Csg.translate (8 + 1.5 + 4 + 2, 0, 10.5 + (H.thickness gearSpec)/2) $ Csg.scale (4 , 40, 8 + 2*2) $ Csg.translate (-0.5, 0, 0) Csg.unitCube 
    clawHolderJoin = (Csg.translate (8, 0, 0) $Csg.scale (20, 50, 20) $ Csg.translate (0.5, 0, -0.5) Csg.unitCube)`Csg.intersection` (Csg.translate (8, 5.5, 0) $ Csg.rotate (1, 0, 0) (pi/2) $ Csg.scale (1.5 + 4 + 2, 1.5 + 4 + 2, 22.5) $ Csg.unitCylinder 32)
    clawHolder = (Csg.unionConcat [clawHolderTop,  clawHolderUnmasked, clawHolderJoin]) `Csg.subtract` clawMask
    bothHolders = clawHolder `Csg.union` (Csg.scale (-1, 1, 1) clawHolder)

    unrotatedMountPlusAttachment = unrotatedMount `Csg.union` bothHolders

    rotatedMount = rotateToServoMount unrotatedMountPlusAttachment
