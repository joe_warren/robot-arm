module ControlPannel  
( controlPannel
, assembly
) where
import qualified Csg
controlPannel :: Csg.BspTree
controlPannel = (Csg.translate center (exterior `Csg.subtract` interior)) `Csg.subtract` 
                    (Csg.unionConcat controlHoles)
  where
    exterior = Csg.scale (110, 145, 50) Csg.unitCube
    interior = Csg.translate (0, 0, -2) $ 
                Csg.scale (110 - 4, 145 - 4, 50 - 2) Csg.unitCube  
    ((x1, y1, z1), (x2, y2, z2)) = Csg.aabb $ Csg.unionConcat controlContents
    center = ((x1 + x2)/2, (y1 + y2)/2, 0)


dialPos = (0, 0, 0)
powerPos = (0, 40 + 25 + 5, 0)
batteryPos = (26 + 21 + 5, 0, 0)
switchPos = (26 + 21 + 5, 40 + 25 + 5, 0)

controlHoles :: [Csg.BspTree]
controlHoles = (uncurry Csg.translate) <$> [
                    (dialPos, dialHoles),
                    (powerPos, powerHoles),
                    (batteryPos, batteryHoles),
                    (switchPos, switchHole)
                  ]
  where
    dialHoles :: Csg.BspTree 
    dialHoles = Csg.unionConcat $ [
                    Csg.translate (i*48, j*70, 0) $ 
                        Csg.scale (1.5, 1.5, 100) $ 
                            Csg.unitCylinder 8
                    | i <- [-0.5, 0.5], j<- [-0.5, 0.5]
                  ] ++ [
                    Csg.scale (52, 80, 2) Csg.unitCube,
                    Csg.translate (-3, 10, 25/2) $ Csg.unionConcat [
                    Csg.translate (i*27/2, j* 17, 0) $
                        Csg.scale (20/2, 20/2, 100) $ 
                            Csg.unitCylinder 16
                        | i <- [-1, 1], j <- [-1,0 ,1] 
                      ], 
                    Csg.translate (9, 10, 25/2) $ Csg.unionConcat [
                    Csg.translate (i*27/2, j* 17, 0) $
                        Csg.scale (10, 10, 100) Csg.unitCube
                        | i <- [-1, 1], j <- [-1,0 ,1] 
                      ], 
                    Csg.translate (-(52/2-8), -(40-15), 10) $ 
                        Csg.scale (12, 22, 100) $ Csg.unitCube
                  ]
    powerHoles = Csg.unionConcat [
                    Csg.translate (i*58, j* 28, 0) $
                        Csg.scale (1.5, 1.5, 100) $
                            Csg.unitCylinder 8
                    | i <- [-0.5, 0.5], j<- [-0.5, 0.5]
                  ]
    switchHole = Csg.scale (7, 7, 100) $ Csg.unitCylinder 16
    batteryHoles = Csg.unionConcat [
                        Csg.translate  (i*20, 0, 0) $
                            Csg.scale (1.5, 1.5, 100) $
                                Csg.unitCylinder 8
                      | i <- [-0.5, 0.5] 
                    ]
controlContents :: [Csg.BspTree]
controlContents = (uncurry Csg.translate) <$> [
                    (dialPos, dials), 
                    (powerPos, powerConverter),
                    (batteryPos, battery),
                    (switchPos, switch)
                  ]
  where 
    dials = Csg.unionConcat [
                Csg.scale (52, 80, 2) Csg.unitCube,
                Csg.translate (-3, 10, 25/2) $ Csg.unionConcat [
                    Csg.translate (i*27/2, j* 17, 0) $
                        Csg.scale (16/2, 16/2, 25) $ 
                            Csg.unitCylinder 16
                    | i <- [-1, 1], j <- [-1,0 ,1] 
                ], 
                Csg.translate (-(52/2-8), -(40-15), 10) $ 
                    Csg.scale (10, 20, 20) $ Csg.unitCube
            ]
    powerConverter = Csg.unionConcat [
        Csg.scale ( 65, 40 ,2) Csg.unitCube, 
        Csg.translate (0, 20, -11) $ Csg.scale (46, 10, 20) Csg.unitCube,
        Csg.translate (0, -20, -11) $ Csg.scale (46, 10, 20) Csg.unitCube
      ]
    battery = Csg.translate (0, 0, -10) $
                 Csg.scale (42, 78, 20) Csg.unitCube
    switch = Csg.unionConcat [
                  Csg.scale (12/2, 12/2, 12*2) $ 
                    Csg.unitCylinder 32 ,
                  Csg.translate (0, 0, -15) $ 
                    Csg.scale (22, 30, 30) Csg.unitCube
                ]
   
assembly = Csg.unionConcat $ controlPannel:controlContents
