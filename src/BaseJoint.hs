module BaseJoint (joint) where

import qualified Csg
import qualified ServoMount

joint :: Csg.BspTree
joint = positives `Csg.subtract` negatives
  where
    baseHeight = 16
    baseRadius = 27
    baseDims = (baseRadius, baseRadius, baseHeight)
    baseUnbeveled = Csg.scale baseDims $ Csg.translate (0, 0, -0.5) $ Csg.unitCylinder 32
    bevelMaskScale = (baseHeight + baseRadius - 4)
    bevelMask = Csg.translate (0, 0, -baseHeight-0.01) $ Csg.uniformScale bevelMaskScale $ Csg.translate (0, 0, 0.5) $  Csg.unitCone 32 
    base = baseUnbeveled `Csg.intersection` bevelMask
    rotatedServoMount = Csg.rotate (-1, 0, 0) (pi/2) ServoMount.rotaryServoMount
    ((_, _, z1), _) = Csg.aabb rotatedServoMount
    servoMount = Csg.translate (0, 6, -z1) rotatedServoMount
    bearingDiameter = 22
    bearingInnerDiameter = 8
    bearingThickness = 7
    bearingInnerDims = (bearingInnerDiameter/2, bearingInnerDiameter/2, 2 * (bearingThickness + baseRadius + 1))
    bearingPaddingDims = (1+(bearingInnerDiameter/2),1+(bearingInnerDiameter/2), 2*(baseRadius + 1))
    bearingAxel = (Csg.scale bearingInnerDims $ Csg.unitCylinder 32) `Csg.union` (Csg.scale bearingPaddingDims $ Csg.unitCylinder 32)
    bearingAxelRotated = Csg.rotate (0, 1, 0) (pi/2) bearingAxel
    bearingRadius = bearingDiameter/2
    bearingAxelPositioned = Csg.translate (0, 0, bearingRadius - (baseHeight+2)) bearingAxelRotated
    bearingAxels = (\a -> Csg.rotate (0, 0, 1) (a*pi/3) bearingAxelPositioned) <$> [0..2]
    
    positives = Csg.unionConcat $ base : servoMount : bearingAxels
    widestCavityDims = (25, 25, 4)
    cavity dims = Csg.translate (0, 0, -baseHeight) $ Csg.scale dims $ Csg.translate (0, 0, 0.5) $ Csg.unitCylinder 32
    widestCavity = cavity widestCavityDims
    axelCavityDims =  (10, 10, 10)
    axelCavity = cavity axelCavityDims
    hornAngleDims = (3.5*(sqrt 2),50/(sqrt 2), baseHeight-1)
    hornAngle = Csg.scale hornAngleDims $Csg.rotate (0, 0, 1) (pi/4) Csg.unitCube
    hornLength = 34
    hornWedge = hornAngle `Csg.intersection` Csg.uniformScale hornLength Csg.unitCube
    hornCrossDims = (17, 3.6, baseHeight -1)
    hornCross = Csg.scale hornCrossDims Csg.unitCube
    hornCavity = Csg.translate (0, 0, -((baseHeight/2) + 0.5)) $ hornWedge `Csg.union` hornCross
    screwHole = Csg.scale (1.25, 1.25, 10) $ Csg.unitCylinder 8
    screwHeadHole = Csg.scale (2.5, 2.5, 10) $ Csg.translate (0, 0, 0.5) $ Csg.unitCylinder 8
    negatives = Csg.unionConcat [widestCavity,  axelCavity, hornCavity, screwHole, screwHeadHole]

